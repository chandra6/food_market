part of 'shared.dart';

Color mainColor = "FFC700".toColor();
Color greyColor = "8D92A3".toColor();

TextStyle greFontSytle = GoogleFonts.poppins().copyWith(color: greyColor);

const double defaultMargin = 24;
